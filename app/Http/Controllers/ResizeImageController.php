<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class ResizeImageController extends Controller
{
    public function resizeImage(Request $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif'
        ]);
        $image = $request->file('image');
        $extension = $image->extension();

        Storage::put('pruebas/' . 'imagenOriginal.'. $extension, $image);

        $imagenRecortado = Image::make($image)->resize(300, 200);
        Storage::put('pruebas/' . 'imagenResize.'. $extension, $imagenRecortado->encode());

        return response()->json('Image uploaded successfully');
    }
}
